/**
 * controllers/LogOutButton.js
 *
 * @file Provides controller for LogOutButton component
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { sleep } from '../utils/utils.js';

/* -------------------------------------------------------------------------- */
/* Controller implementation */

/**
 * Controller for LogOutButton component
 */
const LogOutButton = {

    /* ---------------------------------- */
    /* tryLogOut */

    /**
     * Attempt to end the current session
     */
    async tryLogOut()
    {
        try
        {
            await m.request( {
                method: 'DELETE',
                url: '/shorten/api/session',
                extract: () => {}
            } );
        }
        catch
        {}
        m.route.set( '/auth' );
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { LogOutButton };
