/**
 * interfaces/ShortenedLinksPanel.js
 *
 * @file Provides interface to state-safe functions of ShortenedLinksPanel controller
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { ShortenedLinksPanel as Controller } from '../controllers/ShortenedLinksPanel.js';

/* -------------------------------------------------------------------------- */
/* Interface implementation */

/**
 * Interface for ShortenedLinkPanel controller
 */
const ShortenedLinksPanel = {

    /* ---------------------------------- */
    /* refresh */

    /**
     * Trigger a full refresh and redraw of the component
     */
    refresh()
    {
        Controller.refresh();
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { ShortenedLinksPanel };
