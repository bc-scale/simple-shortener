/**
 * components/LogOutButton.js
 *
 * @file Provides a "Log out" button component that ends the current session
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { LogOutButton as Controller } from '../controllers/LogOutButton.js';

/* -------------------------------------------------------------------------- */
/* Component implementation */

const LogOutButton = {

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        return m( 'button.LogOutButton.button.is-link.is-inverted', {
            disabled: attrs.disabled,
            onclick: Controller.tryLogOut
        }, [
            m( 'i.fa.fa-sign-out-alt')
        ] );
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { LogOutButton };
