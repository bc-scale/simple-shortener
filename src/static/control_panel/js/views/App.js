/**
 * views/App.js
 *
 * @file Provides top-level component for app screen
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { LinkDeletionModal } from '../components/LinkDeletionModal.js';
import { LinkOptionsModal } from '../components/LinkOptionsModal.js';
import { LogOutButton } from '../components/LogOutButton.js';
import { ShortenLinkCard } from '../components/ShortenLinkCard.js';
import { ShortenedLinksPanel } from '../components/ShortenedLinksPanel.js';

/* -------------------------------------------------------------------------- */
/* Helper functions */

/* ---------------------------------- */
/* linkActionModal */

/**
 * Return the appropriate action modal for the currently selected action
 *
 * @param {string} action - The name of the currently selected action
 * @param {string} linkID - The ID of the link that will be operated on
 *
 * @returns {?m.Vnode} - The Vnode representing the appropriate modal
 */
function linkActionModal( action, linkID )
{
    switch( action )
    {
        case 'delete':
        return m( '.modal.is-active', [
            m( '.modal-background' ),
            m( LinkDeletionModal, { linkID: linkID } )
        ] );
        case 'options':
        return m( '.modal.is-active', [
            m( '.modal-background' ),
            m( LinkOptionsModal, { linkID: linkID } )
        ] );
        default:
        return null;
    }
}

/* -------------------------------------------------------------------------- */
/* Component implementation */

const App = {

    /* ---------------------------------- */
    /* Initial state */

    sessionCheckInterval: null,

    /* ---------------------------------- */
    /* oninit */

    /**
     * Called when the component is initialized
     *
     * @param {m.VnodeDOM} vnode - The VnodeDOM object representing the component
     */
    oninit: ( { attrs, state } ) => {

        // Every 60 seconds, make sure that the session is still valid
        state.sessionCheckInterval = setInterval( async () => {
            try
            {
                await m.request( '/shorten/api/session' );
            }
            catch
            {
                m.route.set( '/auth' );
            }
        }, 60000 );

    },

    /* ---------------------------------- */
    /* onremove */

    /**
     * Called before component element is removed from the DOM
     *
     * @param {m.VnodeDOM} vnode - The VnodeDOM object representing the component
     *
     * @returns {Promise} A promise that will resolve once the component may be detached
     */
    onremove: ( { state } ) => {
        clearInterval( state.sessionCheckInterval );
    },

    /* ---------------------------------- */
    /* view */

    /**
     * Called whenever the component is drawn
     *
     * @param {m.Vnode} vnode - The Vnode representing the component
     *
     * @returns {m.Vnode} - The Vnode or Vnode tree to be rendered
     */
    view: ( { attrs } ) => {
        const modal = linkActionModal( attrs.action, attrs.linkID );
        return [
            modal,
            m( 'main.App', [
                m( LogOutButton, { disabled: modal !== null } ),
                m( 'h1.title', 'Link shortener' ),
                m( 'h1.subtitle', window.location.hostname ),
                m( '.columns.is-desktop', [
                    m( ShortenLinkCard, { disabled: modal !== null } ),
                    m( ShortenedLinksPanel, { disabled: modal !== null } )
                ] ),
            ] )
        ];;
    }

}

/* -------------------------------------------------------------------------- */
/* Export */

export { App };
