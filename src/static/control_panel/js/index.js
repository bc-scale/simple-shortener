/**
 * index.js
 *
 * @file Application entry point
 *
 * @author Jessie Hildebrandt
 */

/* -------------------------------------------------------------------------- */
/* Local module imports */

import { App } from './views/App.js';
import { Auth } from './views/Auth.js';

/* -------------------------------------------------------------------------- */
/* Route resolvers */

/**
 * Checks for an active session and redirects the client to the app route if one already exists.
 * Otherwise, resolves to the Auth view.
 */
const authResolver = {
    async onmatch()
    {
        try
        {
            await m.request( '/shorten/api/session' );
            m.route.set( '/app' );
        }
        catch
        {
            return Auth;
        }
    }
};

/**
 * Checks for an active session and redirects the client to the auth route if one doesn't exist.
 * Otherwise, resolves to the App view.
 */
const appResolver = {

    async onmatch( args )
    {

        try
        {

            await m.request( '/shorten/api/session' );

            if ( 'action' in args )
            {

                // Make sure that if an action was provided, that it is one that is supported
                switch ( args.action )
                {

                    case 'options':
                    case 'delete':
                    break;

                    default:
                    m.route.set( '/app', null, { replace: true } );
                    return;

                }

                // Make sure that the link we're performing an action on actually exists
                const response = await m.request( `/shorten/api/links/${ args.linkID }` );
                const link = response.find( link => link.link_id === args.linkID );
                if ( !link )
                {
                    m.route.set( '/app', null, { replace: true } );
                    location.reload();
                }

            }

            return App;

        }
        catch
        {
            m.route.set( '/auth' );
        }

    }

};

/* -------------------------------------------------------------------------- */
/* Routes */

m.route( document.body, '/auth', {
    '/auth': authResolver,
    '/app': appResolver,
    '/app/:action/:linkID': appResolver,
} );

/* -------------------------------------------------------------------------- */
/* Update window title */

document.title = `${ window.location.host } | Link Shortener Control Panel`
